import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by wang_x1 on 24/03/15.
 */
public class ColorTableCellRender extends DefaultTableCellRenderer implements TableCellRenderer {

    private Color _background = getBackground();
    private int running = -1;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JComponent cell = (JComponent) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (row == running)
            cell.setBackground(Color.red);
        else
            cell.setBackground(_background);
        return this;
    }

    public void setRunning(int row) {
        this.running = row;
    }
}
