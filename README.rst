The matlab uitable component has limited functions.
It can be custimized though java code, in `an undocumented way <http://undocumentedmatlab.com/blog/uitable-cell-colors>`_

This a simple program to set background color of a specific row.

Steps to run the demo:

1. Compile the java program. Use the same version of jave as the one in Matlab.
   
   1. Check matlab java version::

        > version -java

    
   2. Check your java in the path::

        $ java -version

   3. Compile into jar::

        $ javac ColorTableCellRender.java
        $ jar cf ColorTableCellRender.jar ColorTableCellRender.class

2. Run the matlab program::

        > untitled
